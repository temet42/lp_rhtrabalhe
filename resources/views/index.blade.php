<!DOCTYPE html>
<html lang="en">
<head>
    <title>RH Trabalhe</title>

    <meta name="keywords" content="rh, rhtrabalhe,trabalho,estagio"/>

    <meta property="og:title" content="Rh Trabalhe" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="http://rhtrabalhe.com.br/" />
    <meta property="og:image" content="http://rhtrabalhe.com.br/images/LogoRH" />


    <meta property="og:locale" content="pt_PT" />


    <meta property="og:description" content="A nossa empresa atua no segmento de integração de estágios, sua regularização e no agenciamento de profissionais, que consiste na seleção de pessoal para efetivação. Novas formas de organização do trabalho, qualidade de vida , práticas de envolvimento dos trabalhadores, comprometimento dos níveis gerenciais, ligação entre a estratégia empresarial e de recursos humanos. " />



    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no"/>
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="css/grid.css">


    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/style2.css">


    <link rel="stylesheet" href="css/contact-form.css">
    <link rel="stylesheet" href="css/jquery.fancybox.css"/>
    <link rel="stylesheet" href="css/owl-carousel.css"/>




    <script src="js/jquery.js"></script>
    <script src="js/jquery-migrate-1.2.1.js"></script>


    <script src="js/sweetalert.min.js"></script>
    <link href='css/sweetalert.css' rel='stylesheet'>




    <!--[if lt IE 9]>
    <html class="lt-ie9">
    <div style=' clear: both; text-align:center; position: relative;'>
        <a href="http://windows.microsoft.com/en-US/internet-explorer/..">
            <img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."/>
        </a>
    </div>
    <script src="js/html5shiv.js"></script><![endif]-->

    <script src='js/device.min.js'></script>



    <script>
        function initMap() {
            var uluru = {lat: -12.8933059, lng: -38.3218789};
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 20,
                center: uluru
            });
            var marker = new google.maps.Marker({
                position: uluru,
                map: map
            });
        }
    </script>

    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVbJKGXocGVC6BcD9tcKrnPp7z9A91xUo&callback=initMap">
    </script>


    <style>
        #map {
            height: 400px;
            width: 100%;
        }
    </style>

    <script type="text/javascript">
        @if(Session::has('flash_message'))
        $(document).ready(function(){
            sweetAlert("Envio Realizado Com Sucesso!", "", "success");
        });
        @endif
        @php
            Session::forget('flash_mesage');
        @endphp

        @if ($errors->has('emailC'))
        $(document).ready(function(){
            sweetAlert("Email já Cadastrado", "", "warning");
        });
        @endif
    </script>





</head>
<body>
<div class="page">
    <!--========================================================
                         HEADER
    =========================================================-->

    <!--modal-->

    <div id="openModal" class="modalDialog  ">
        <div>
            <a href="#close" title="Close" class="closeModal"></a>
            <!-- Conteúdo do Modal -->
            <h2 class="h2">Cadastro Currículo</h2>
            {!! Form:: open(['url'=>'/curriculo', 'method' => 'POST','enctype=multipart/form-data' ]) !!}
            {{ csrf_field() }}
            <hr>

            <div class="pgContato  ">
                <div class="contato  ">
                    <div class="formContato "><br>

                        <input type="text" id="nome" name="nomeC" placeholder="Nome" required />
                        <input type="text" id="emailC" name="emailC" placeholder="Email" required />
                        <input type="text" id="telefone" name="telefoneC" placeholder="Telefone" required/>
                        <!-- <input  type="file" name="curriculo" >-->


                        <select name="modalidade" class="sizecampo"  id="modalidade" >
                            <option value=" selected">&nbsp;&nbsp;&nbsp;&nbsp;Area de Atuação</option>
                            <option value="Motorista">&nbsp;&nbsp;&nbsp;&nbsp;Motorista</option>
                            <option value="Gráfica" >&nbsp;&nbsp;&nbsp;&nbsp;Gráfica</option>
                            <option value="Empresa">&nbsp;&nbsp;&nbsp;&nbsp;Empresa</option>

                        </select><br><br>

                      <!--  <div class="g-recaptcha size1" data-sitekey="6Lduh1QUAAAAAJVAIMTkHQLkpq3TmeBpKnivEC8C"></div>-->


                        <button type="submit" class="botaoContato " >Enviar</button>
                        {!!Form:: close()!!}
                    </div>
                    &nbsp; &nbsp; <label>OBS: Envie  o seu curriculo  para rhtrabalhe@gmail.com</label>
                    <!-- Conteúdo do Modal -->


                </div>
            </div>
        </div>
    </div>

    <!-- /.modal -->

    <!--modal2-->

    <div id="openModal2" class="modalDialog  ">
        <div>
            <a href="#close" title="Close" class="closeModal"></a>
            <!-- Conteúdo do Modal -->
            <h2 class="h2">Cadastro Empresa</h2>
            <hr>
            {!! Form:: open(['url'=>'/empresa', 'method' => 'POST']) !!}

            <div class="pgContato  ">
                <div class="contato  ">
                    <div class="formContato "><br>

                        <input type="text" id="nome" name="nomeE" placeholder="Razão Social" required />
                        <input type="text" id="email" name="emailE" placeholder="Email" required />
                        <input type="text" id="telefone" name="telefoneE" placeholder="Telefone" required/>

                      <br><br>
                       <!-- <div class="g-recaptcha size1" data-sitekey="6Lduh1QUAAAAAJVAIMTkHQLkpq3TmeBpKnivEC8C"></div>-->


                        <button type="submit" class="botaoContato " >Enviar</button>

                        {!!Form:: close()!!}
                    </div>
                    <!-- Conteúdo do Modal -->


                </div>
            </div>
        </div>
    </div>

    <!-- /.modal2-->


    <header>
        <div class="container">
            <div class="brand">
                <img class="brand_logo" src="images/logo.png" alt=""/>

                <h1 class="brand_name">
                    <a href="./">RH TRABALHE</a>
                </h1>
            </div>
            <address class="call">Contato:(71)9 8828-6608
            </address>
        </div>

        <ul class="social-list">
            <li>
                <a class="fa fa-facebook" href="#"></a>
            </li>
           <!-- <li>
                <a class="fa fa-twitter" href="#"></a>
            </li>-->
            <li>
                <a class="fa fa-instagram" href="#"></a>
            </li>
            <li>
                <a class="fa fa-envelope" href="#"></a>
            </li>
        </ul>
        <br>
        <div class="container">
            <h2 class="center "> Os Melhores profissionais para o seu  serviço</h2>


            <div class="center offs1">
                <a href='#openModal' class="btn btn1">Cadastre o seu Currículo</a>
            </div>
        </div>
    </header>
    <!--========================================================
                                                        CONTENT
    =========================================================-->
    <main>
        <section class="well">
            <div class="container">
                <div class="center row">
                    <h3>Nosso publico alvo</h3>
                    <p></p>
                    <ul class="list">
                        <li class="wow fadeIn" data-wow-duration="1.0s">

                                <img src="images/page-1_img01.png" alt=""/>


                            <p>Atendente</p>
                        </li>
                        <li class="wow fadeIn" data-wow-duration="1.5s">

                                <img src="images/page-1_img02.png" alt=""/>


                            <p>Estágiario</p>
                        </li>
                        <li class="wow fadeIn" data-wow-duration="2.0s">

                                <img src="images/page-1_img03.png" alt=""/>


                            <p>Vendas</p>
                        </li>
                        <li class="wow fadeIn" data-wow-duration="2.5s">

                                <img src="images/page-1_img04.png" alt=""/>


                            <p>Secretária</p>
                        </li>
                        <li class="wow fadeIn" data-wow-duration="3.0s">

                                <img src="images/page-1_img05.png" alt=""/>


                            <p>Outros</p>
                        </li>
                    </ul>
                </div>
            </div>

        </section>
        <section class="well2  well7 bg-primary">
            <div class="container">
                <div class="row">
                    <div class="grid_7">
                        <h4>COMO FUNCIONA?</h4>

                        <p>Para solicitar candidatos para estágio ou emprego ou realizar um cadastro para as vagas ofertadas, preencha o
                             formulário disponibilizado pelo Site e aguarde, entraremos em contato.


                        </p>
                        <br><br><br>
                        <ul class="list2">
                            <li>
                                <h2 class="fa fa-calendar">10<span>Empresas</span></h2>
                            </li>
                            <li>
                                <h2 class="fa fa-clock-o">15<span>Candidatos</span></h2>
                            </li>
                            <li>
                                <h2 class="fa  fa-group">20<span>Vagas</span></h2>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <img class="human " src="images/page-1_img06.png" alt=""/>
        </section>

        <section class="well3">
            <!--   <div class="container">
                  <div class="row">
                      <div class="box">
                          <div class="box_aside">
                              <a class="thumb fancybox.iframe" href="//player.vimeo.com/video/37582125?wmode=transparent">
                                  <img src="images/page-1_img07.jpg" alt=""/>

                                  <div class="thumb_overlay thumb_overlay_video">
                                      <h2><span>Oil & Gas Expert</span>Jianna Hola</h2>
                                  </div>
                              </a>
                          </div>
                         <div class="box_cnt">
                              <h4 class="regular">Case study</h4>
                              <ul class="marked-list">
                                  <li class="wow fadeInRight" data-wow-duration="1.0s">
                                      <a href='#'>Sed ut perspiciatis unde omnis iste natus error</a>
                                  </li>
                                  <li class="wow fadeInRight" data-wow-duration="1.5s">
                                      <a href='#'>Voluptatem accusantium doloremque laudant </a>
                                  </li>
                                  <li class="wow fadeInRight" data-wow-duration="2.0s">
                                      <a href='#'>Totam rem aperiam, eaque ipsa quae ab illo i</a>
                                  </li>
                                  <li class="wow fadeInRight" data-wow-duration="2.5s">
                                      <a href='#'>Nventore veritatis et quasi architecto beatae </a>
                                  </li>
                                  <li class="wow fadeInRight" data-wow-duration="3.0s">
                                      <a href='#'>Vitae dicta sunt explicabo. Nemo enim ipsam </a>
                                  </li>
                              </ul>
                              <a href='#' class="btn btn2">See all videos</a>
                          </div>
                      </div>
                  </div>
              </div>-->
            <div class="container center">


                <div class="row offs2">
                    <div class="grid_4 price-box">
                        <h6>Empresa
                        </h6>
                        <ul>
                            <li>Escritorio Virtual Interativo</li>
                            <li>Recrutamento e Seleção de Pessoa</li>
                            <li>Mão de obra efetiva e Temporária</li>
                            <li>Acompanhamento Administrativo</li>
                            <li>Segurança na Contratação</li>
                            <li>Terapeutas disponiveis para as empresas contratadas</li>
                        </ul>

                    </div>
                    <div class="grid_4 price-box">
                        <h6>Profissionais
                        </h6>
                        <ul>
                            <li>Escritório virtual interativo</li>
                            <li>Disponibilidade de Vagas</li>
                            <li>Excelentes Vagas de Emprego</li>
                            <li>Orientações Profissionais</li>
                            <li>Dicas para Entrevista</li>
                            <li></li>
                            <br> <br> <br>

                        </ul>

                    </div>
                    <div class="grid_4 price-box">
                        <h6>Estudante
                        </h6>
                        <ul>
                            <li>Escritório Interativo</li>
                            <li>Disponibilidade de Vagas</li>
                            <li>Site Virtual Interativo</li>
                            <li>Excelentes Vagas de Estágio</li>
                            <li>Orientações Profissionais</li>
                            <li>Dicas para Entrevista</li>
                            <br> <br>
                        </ul>
                    </div>
                </div>
            </div>
        </section>



        <section class="  bg-secondary2">
            <div class="container">
                <div class="row">
                    <div class="container center"><br>
                        <h3 class="white">GRUPO RH TRABALHE </h3><br>
                        <p class="font1">

                            A nossa empresa atua no segmento de integração de estágios,
                            sua regularização e no agenciamento de profissionais,
                            que consiste na seleção de pessoal para efetivação.

                            Novas formas de organização do trabalho, qualidade de vida , práticas de
                            envolvimento dos trabalhadores, comprometimento dos níveis gerenciais, ligação entre a
                            estratégia empresarial e de recursos humanos.


                        </p>
                        <br><br>

                    </div>

                </div>
            </div>

        </section>


        <!--  <section class="well4 bg-secondary2">
              <div class="container center">
                  <div class="row">

                      <div class="owl-carousel">
                          <div class="item box2 grid_10">
                              <h3 class="posetxt">GRUPO RH TRABALHE</h3>
                              <div class="box2_aside">

                                  <img src="images/img1.jpg" alt=""/>
                              </div>
                              <div class="box2_cnt ">

                                  <blockquote>

                                      <p>
                                          <q>
                                              Novas formas de organização do trabalho, qualidade de vida no trabalho, práticas de
                                              envolvimento dos trabalhadores, comprometimento dos níveis gerenciais, ligação entre a
                                              estratégia empresarial e de recursos humanos.<br>

                                          </q>
                                      </p>
                                     <!-- <cite> George Lukas <em>CEO Lukas Tractors</em> </cite>
                                  </blockquote>
                              </div>
                          </div>
                          <!--<div class="item box2 grid_10">
                              <h3 class="posetxt">EMPRESA</h3>
                              <div class="box2_aside">
                                  <img src="images/img2.jpg" alt=""/>
                              </div>
                              <div class="box2_cnt">
                                  <blockquote>
                                      <p>
                                          <q>     Escritório virtual Interativo,
                                              Recrutamento e  Seleção de Pessoal: Estagiário; Mão de obra efetiva e Temporária,
                                              Acompanhamento Administrativo,
                                              Menor Custo/Beneficio,
                                              Segurança na Contratação,
                                              Terapeutas disponívies para as empresas cadastradas.
                                               </q>
                                      </p>
                                      <h4>
                                         <!-- <cite>George Lukas <em>CEO Lukas Tractors</em> </cite>
                                      </h4>
                                  </blockquote>
                              </div>
                          </div>
                          <div class="item box2 grid_10">
                              <h3 class="posetxt">PROFISSIONAIS</h3>
                              <div class="box2_aside">
                                  <img src="images/img3.jpg" alt=""/>
                              </div>
                              <div class="box2_cnt">
                                  <blockquote>
                                      <p>
                                          <q> Escritório virtual Interativo,
                                              Disponibilidade de Vagas,
                                              Excelentes vagas deEmprego,
                                              Orientações Profissionais,
                                              Dicas para entrevista.

                                          </q>
                                      </p>
                                      <h4>
                                         <!-- <cite>George Lukas <em>CEO Lukas Tractors</em> </cite>
                                      </h4>
                                  </blockquote>
                              </div>
                          </div>
                        <div class="item box2 grid_10">
                              <h3 class="posetxt">ESTUDANTES</h3>
                              <div class="box2_aside">
                                  <img src="images/img4.jpg" alt=""/>
                              </div>
                              <div class="box2_cnt">
                                  <blockquote>
                                      <p>
                                          <q>     Escritório virtual Interativo,
                                              Disponibilidade de Vagas,
                                              Site virtual Interativo,
                                              Excelentes vagas de Estágio,
                                              Orientações Profissionais,
                                              Dicas para entrevista,
                                              Direitos do Estagiário. </q>
                                      </p>
                                      <h4>
                                          <!-- <cite>George Lukas <em>CEO Lukas Tractors</em> </cite>
                                      </h4>
                                  </blockquote>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </section>-->
        <br>


        <section class="well5 parallax parallax_skins1" data-url="images/parallax2.jpg" data-mobile="true" data-speed="0.5" data-direction="inverse" data-blur="0.8">
            <div class="container center">
                <h2 class="white">Cadastre a sua empresa  </h2><br>
                <h6 class="white">Solicite candidatos a estágio/emprego agenciados pela RH TRABALHE</h6>

                <a href='#openModal2' class="btn btn1 btn1_mod1">Cadastro</a>
            </div>
        </section>
        <br>

        <section class="  bg-primary">
            <div class="container">
                <div class="row">
                    <div class="container center"><br>
                        <h2 class="white">Serviços Contabeis e Escritório Virtual  </h2><br>
                        <p>Em parceria com a empresa Centro Virtual, a RH Trabalhe oferece serviços de contabilidade como:
                            <br>
                            <br>
                            Fiscal, Pessoal, Contabil
                            <br>
                            Abertura, Alteração e Baixa de Empresa
                            <br><br>
                            Para mais informações
                            Tel:3508.2238 ou
                            3482.5251

                        </p>
                        <br>
                    </div>

                </div>
            </div>

        </section>



        <section id="contact-us" class="well2">

            <div class="container center">
                <h2>Fale Conosco</h2>
                <p>Duvidas? Preencha o formulario abaixo  e entratremos em contato. </p>

                {!! Form:: open(['url'=>'/', 'method' => 'POST'])  !!}
                <div class="rd-form rd-mailform">

                    <div class="row">
                        <div class="grid_6">
                            <div class="form-wrap">
                                <input class="form-input" id="contact-name" type="text" name="nome" data-constraints="@Required" required>
                                <label class="form-label" for="contact-name">Nome</label>
                            </div>
                        </div>
                        <div class="grid_6">
                            <div class="form-wrap">
                                <input class="form-input" id="contact-phone" type="text" name="telefone" data-constraints="@PhoneNumber @Required" required>
                                <label class="form-label" for="contact-phone">Telefone</label>
                            </div>
                        </div>

                        <div class="grid_12">
                            <div class="form-wrap">
                                <label class="form-label" for="contact-message">Menssagem</label>
                                <textarea class="form-input" id="contact-message" name="mensagem" data-constraints="@Required" required></textarea>
                            </div>
                        </div>
                        <div class="grid_6">
                            <div class="form-wrap">
                                <input class="form-input" id="contact-email" type="email" name="email" data-constraints="@Email @Required" required>
                                <label class="form-label" for="contact-email">E-mail</label>
                            </div>
                        </div>

                        <div class="grid_6">
                            <button class="btn btn1 btn1_mod1" type="submit">Enviar</button>
                        </div>


                    </div>
                    {!!Form:: close()!!}
                </div>

            </div>
        </section>




        <div id="map"></div>

        <br>

    </main>

    <!--========================================================
                                                        FOOTER
    =========================================================-->
    <footer class="well6">
        <div class="container">
            <div class="left">
                <p>Nossas redes sociais</p>
                <ul class="inline-list">
                    <li>
                        <a class="fa fa-facebook-square" href="#"></a>
                    </li>
                    <li>
                        <a class="fa  fa-linkedin-square" href="#"></a>
                    </li>
                    <li>
                        <a class="fa fa-instagram" href="#"></a>
                    </li>
                   <!-- <li>
                        <a class="fa  fa-pinterest-square" href="#"></a>
                    </li>-->
                  <!--  <li>
                        <a class="fa fa-twitter-square" href="#"></a>
                    </li>-->
                </ul>
            </div>
            <div class="right">
                <strong><span id="copyright-year"></span></strong> <strong>Desenvolvido por <span><a href="http://temet.com.br/" target="_blank">TEMET</a> </span> </strong>
            </div>
        </div>
        <!-- {%FOOTER_LINK} -->
    </footer>
</div>
<!-- Global Mailform Output-->
<div class="snackbars" id="form-output-global"></div>
<!-- Javascript-->
<script src="js/rd-mailform.js"></script>
<script src="js/script.js"></script>
<!-- coded by Belkins -->
</body>
</html>
