<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class curriculo extends Model
{
    protected $table = 'curriculo';
    protected $fillable = ['nome', 'email', 'telefone', 'arquivo','modalidade'];
}
