<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FaleConosco extends Model
{

    protected $table = 'faleconosco';
    protected $fillable = ['nome', 'email', 'telefone', 'mensagem'];




}
