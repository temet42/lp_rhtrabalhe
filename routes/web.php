<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Illuminate\Http\Request;



Route::get('/', function () {
    return view('index');
});


//--------------------------  CURRICULO -----------------------------------------

Route::match(['resource','post'],'/curriculo' ,function (Request $request) {

    $user = new \App\curriculo();
    $request->validate([
        'nome'        =>   'required|min:3|max:100',
        'telefone'    =>    'required|numeric',
        'modalidade'  =>    'required',
        'email'       =>    'required|email|unique:curriculo'
    ]);

    // inserir banco de dados

    $novo = new \App\curriculo();
    $novo->nome = $request->get('nomeC');
    $novo->email = $request->get('emailC');
    $novo->telefone = $request->get('telefoneC');
    $novo->arquivo = $request->file('curriculo');
    $novo->modalidade = $request->get('modalidade');
    $novo->save();

    //ENVIAR EMAIL PARA RHTRABALHE

    Mail::send('contatoCliente', ['request' => $request], function ($msg) {
        $msg->subject('Cliente');
        $msg->to('contatorhtrabalhe@gmail.com');
    });


    \Session::flash('flash_message', 'algo');
    return Redirect::to('/');
});


// ----------------------------  Fale conosco ------------------------------------

Route::match(['resource','post'],'/' ,function (Request $request) {


    // inserir banco de dados

  $novo = new \App\FaleConosco();
    $novo->nome = $request->get('nome');
    $novo->email = $request->get('email');
    $novo->telefone = $request->get('telefone');
    $novo->mensagem = $request->get('mensagem');
    $novo->save();


    //ENVIAR EMAIL PARA RHTRABALHE

    Mail::send('contato', ['request' => $request], function ($msg) {
        $msg->subject('Cliente');
        $msg->to('contatorhtrabalhe@gmail.com');
    });


    \Session::flash('flash_message', 'algo');
    return Redirect::to('/');
});



// --------------------------------  EMPRESA -------------------------------------

Route::match(['resource','post'],'/empresa' ,function (Request $request) {

    // inserir banco de dados

    $novo = new \App\empresa();
    $novo->nome = $request->get('nomeE');
    $novo->email = $request->get('emailE');
    $novo->telefone = $request->get('telefoneE');
    $novo->save();


    //ENVIAR EMAIL PARA RHTRABALHE

    Mail::send('contatoEmpresa', ['request' => $request], function ($msg) {
        $msg->subject('Empresa Prospectada');
        $msg->to('contatorhtrabalhe@gmail.com');
    });
    \Session::flash('flash_message', 'algo');
    return Redirect::to('/');
});